"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");

const app = express();

function normalizePort(val) {
  const port = parseInt(val, 10);
  if (isNaN(port)) {
    return val;
  }
  if (port >= 0) {
    return port;
  }
  return false;
}

const port = normalizePort(process.argv.slice(2) || 3000);
app.use(bodyParser.json({ type: ["json", "+json"] }));

// Write files
app.post("/writeFiles", async (req, res) => {
  try {
    fs.writeFileSync(`storage/${req.body.fileName}`, req.body.content);
    res.status(200).send("Data written successfully!");
  } catch (err) {
    res.status(500).send(err);
  }
});

// Read files
app.get("/readFile", async (req, res) => {
  fs.readFile(`storage/${req.query.fileName}`, "utf8", (err, data) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(200).send(data);
  });
});

// Append files
app.post("/appendFile", async (req, res) => {
  fs.appendFile(
    `storage/${req.body.fileName}`,
    req.body.content,
    "utf8",
    (err) => {
      if (err) {
        return res.status(500).send(err);
      }
      res.status(200).send("Data is appended to file successfully.");
    }
  );
});

// Delete files
app.delete("/deleteFile", async (req, res) => {
  fs.unlink(`storage/${req.query.fileName}`, (err) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(200).send("File deleted successfully!");
  });
});

app.listen(port, () => console.log(`"app listening on port ${port}!"`));
