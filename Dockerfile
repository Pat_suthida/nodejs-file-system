FROM node:18-alpine
WORKDIR /app
COPY ["package.json", "app.js", "/app/"]

RUN npm install

COPY . .

CMD ["npm", "start"]